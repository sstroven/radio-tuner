package com.sstroven.autoradiov2;

//import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View.OnClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.android.gms.car.CarRadioManager;
import com.google.android.gms.car.RadioProperties;
import com.google.android.gms.car.RadioState;
import com.google.android.gms.car.RadioStationInfo;
import com.google.android.gms.car.support.Fragment;

import java.util.Random;

import android.view.ViewGroup.LayoutParams;
import android.widget.*;

/**
 * A placeholder fragment containing a simple view.
 */
public class RadioActivityFragment extends Fragment {
    private RadioActivity mRadioActivity;
    private View mView;
    //private View mSourceView;
    private SharedPreferences mSharedPref;
    private SharedPreferences.Editor mPrefEdit;
    private static final String PREFERENCE_NAME = "AutoRadioData";
    private static final int MODE_PRIVATE = 0;
    private String mRadioSelected;
    private float mStationSelected;
    private final int PRESET_COUNT = 8;
    private View mPresetViews[] = new View[PRESET_COUNT];
    private View mPresetTypeViews[] = new View[PRESET_COUNT];
    private final String KEY_LAST_STATION = "last_station";
    private final String KEY_LAST_RADIO = "last_radio";
    private final String KEY_LAST_STATION_FM = "last_station_FM";
    private final String KEY_LAST_STATION_AM = "last_station_AM";

    private CarRadioManager mRadio;
    private RadioProperties mRadioProperties;
    private RadioStationInfo mRadioStationInfo;

    public RadioActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        int temp = -123;
        mRadioActivity = (RadioActivity) getActivity();

        mView = inflater.inflate(R.layout.fragment_radio, container, false);
        //mSourceView = inflater.inflate(R.layout.fragment_source, container, false);

        mSharedPref = mRadioActivity.getContext().getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE);
        mPrefEdit = mSharedPref.edit();

        mStationSelected = mSharedPref.getFloat(KEY_LAST_STATION, -1);
        mRadioSelected = mSharedPref.getString(KEY_LAST_RADIO, "No Radio");

        if(mStationSelected != -1) {
            TextView textView = (TextView) mView.findViewById(R.id.station_number);
            textView.setText(Float.toString(mStationSelected));
            textView = (TextView) mView.findViewById(R.id.radio_type);
            textView.setText(mRadioSelected);

            //testing start
            //try {
            //    mRadio.seek(2, true, false);
            //}
            //catch (com.google.android.gms.car.CarNotConnectedException e){
            //}
            //temp = mRadioStationInfo.channel;
            //textView.setText(Integer.toString(temp));
            //testing end
        }
        loadPresets();

        Button button;
        /**Button button = (Button) mView.findViewById(R.id.FM_button);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setRadioFM(view);
            }
        });
        button = (Button) mView.findViewById(R.id.AM_button);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                setRadioAM(view);
            }
        });*/

        button = (Button) mView.findViewById(R.id.radio_sources);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoRadioSources(view);
            }
        });
        button = (Button) mView.findViewById(R.id.seek_forward);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                seekForward(view);
            }
        });
        button = (Button) mView.findViewById(R.id.seek_backward);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                seekBackward(view);
            }
        });
        button = (Button) mView.findViewById(R.id.scan_forward);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                scanForward(view);
            }
        });
        button = (Button) mView.findViewById(R.id.scan_backward);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                scanBackward(view);
            }
        });


        return mView;
    }

    @Override
    public void onStop() {
        super.onStop();
        mPrefEdit.putFloat(KEY_LAST_STATION, mStationSelected);
        mPrefEdit.putString(KEY_LAST_RADIO, mRadioSelected);
        mPrefEdit.commit();
    }

    public void setRadioFM(View view) {
        TextView textView = (TextView) mView.findViewById(R.id.radio_type);
        textView.setText(R.string.radio_FM);
        mRadioSelected = "FM";
        textView = (TextView) mView.findViewById(R.id.station_number);
        mStationSelected = mSharedPref.getFloat(KEY_LAST_STATION_FM, (float)88.7);
        textView.setText(Float.toString(mStationSelected));
    }

    public void setRadioAM(View view) {
        TextView textView = (TextView) mView.findViewById(R.id.radio_type);
        textView.setText(R.string.radio_AM);
        mRadioSelected = "AM";
        textView = (TextView) mView.findViewById(R.id.station_number);
        mStationSelected = mSharedPref.getFloat(KEY_LAST_STATION_AM, 330);
        textView.setText(Float.toString(mStationSelected));
    }

    /**private void selectRadioFM(View view) {
        String radioType = mSharedPref.getString(KEY_LAST_RADIO, "no radio");
        if(radioType.equals("no radio") || radioType.equals("AM")) {
            float station = mSharedPref.getFloat(KEY_LAST_STATION_FM, (float)88.7);
            mPrefEdit.putFloat(KEY_LAST_STATION, station);
            mPrefEdit.putString(KEY_LAST_RADIO, "FM");
            mPrefEdit.commit();
        }
    }

    private void selectRadioAM(View view) {
        String radioType = mSharedPref.getString(KEY_LAST_RADIO, "no radio");
        if(radioType.equals("no radio") || radioType.equals("FM")) {
            float station = mSharedPref.getFloat(KEY_LAST_STATION_AM, (float)330);
            mPrefEdit.putFloat(KEY_LAST_STATION, station);
            mPrefEdit.putString(KEY_LAST_RADIO, "AM");
            mPrefEdit.commit();
        }
    }*/

    public void gotoRadioSources(View view) {
        final LinearLayout radioFragment = (LinearLayout) mView.findViewById(R.id.radio_fragment);
        final LinearLayout sourceFragment = (LinearLayout) mView.findViewById(R.id.source_fragment);
        radioFragment.setVisibility(mView.INVISIBLE);
        sourceFragment.setVisibility(mView.VISIBLE);

        Button button = (Button) mView.findViewById(R.id.radio_select_FM);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //selectRadioFM(view);
                radioFragment.setVisibility(mView.VISIBLE);
                sourceFragment.setVisibility(mView.INVISIBLE);
                setRadioFM(view);
            }
        });
        button = (Button) mView.findViewById(R.id.radio_select_AM);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //selectRadioAM(view);
                radioFragment.setVisibility(mView.VISIBLE);
                sourceFragment.setVisibility(mView.INVISIBLE);
                setRadioAM(view);
            }
        });
        button = (Button) mView.findViewById(R.id.radio_select_back);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                radioFragment.setVisibility(mView.VISIBLE);
                sourceFragment.setVisibility(mView.INVISIBLE);
            }
        });
        /**LayoutParams params;
        boolean click = true;

        final PopupWindow popUp = new PopupWindow(mRadioActivity.getContext());
        LinearLayout layout = new LinearLayout(mRadioActivity.getContext());
        final LinearLayout mainLayout = new LinearLayout(mRadioActivity.getContext());
        TextView tv = new TextView(mRadioActivity.getContext());
        Button but = new Button(mRadioActivity.getContext());

        params = new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        layout.setOrientation(LinearLayout.VERTICAL);
        tv.setText("Hi this is a sample text for popup window");
        layout.addView(tv, params);
        popUp.setContentView(layout);
        // popUp.showAtLocation(layout, Gravity.BOTTOM, 10, 10);
        mainLayout.addView(but, params);
        mRadioActivity.setContentView(mSourceView);*/
    }

    /**public void getHistory(View view) {
     PopupWindow popup = new PopupWindow(this);
     LinearLayout layout = new LinearLayout(this);
     LinearLayout mainlayout = new LinearLayout(this);
     ViewGroup.LayoutParams params;
     TextView textView = new TextView(this);

     //Button button = new Button(this);
     //button.setText("Done");
     popup.showAtLocation(mainlayout, Gravity.CENTER, 10, 10);
     popup.update(50, 50, 300, 80);

     params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
     layout.setOrientation(LinearLayout.HORIZONTAL);
     textView.setText("HELLO");
     layout.addView(textView, params);
     }*/

    public void scanForward(View view) {
        TextView textView = (TextView) mView.findViewById(R.id.station_number);

        //temporary simulation
        Random rand = new Random();
        if(mRadioSelected.equals("FM")) {
            mStationSelected = rand.nextInt(30) + 80;
            textView.setText(Float.toString(mStationSelected));
            mPrefEdit.putFloat(KEY_LAST_STATION_FM, mStationSelected);
            mPrefEdit.commit();
        }
        else if(mRadioSelected.equals("AM")) {
            mStationSelected = rand.nextInt(340) + 100;
            textView.setText(Float.toString(mStationSelected));
            mPrefEdit.putFloat(KEY_LAST_STATION_AM, mStationSelected);
            mPrefEdit.commit();
        }
    }

    public void scanBackward(View view) {
        TextView textView = (TextView) mView.findViewById(R.id.station_number);

        //temporary simulation
        Random rand = new Random();
        if(mRadioSelected.equals("FM")) {
            mStationSelected = rand.nextInt(30) + 80;
            textView.setText(Float.toString(mStationSelected));
            mPrefEdit.putFloat(KEY_LAST_STATION_FM, mStationSelected);
            mPrefEdit.commit();
        }
        else if(mRadioSelected.equals("AM")) {
            mStationSelected = rand.nextInt(340) + 100;
            textView.setText(Float.toString(mStationSelected));
            mPrefEdit.putFloat(KEY_LAST_STATION_AM, mStationSelected);
            mPrefEdit.commit();
        }
    }

    public void seekForward(View view) {
        TextView textView = (TextView) mView.findViewById(R.id.station_number);

        //temporary simulation
        Random rand = new Random();
        /**if (radioSelected.equals("FM")) {
         stationSelected = rand.nextInt(30) + 80;
         textView.setText(Float.toString(stationSelected));
         } else if (radioSelected.equals("AM")) {
         stationSelected = rand.nextInt(340) + 100;
         textView.setText(Float.toString(stationSelected));
         }*/
        scanForward(view);
    }

    public void seekBackward(View view) {
        TextView textView = (TextView) mView.findViewById(R.id.station_number);

        //temporary simulation
        /**Random rand = new Random();
         if (radioSelected.equals("FM")) {
         stationSelected = rand.nextInt(30) + 80;
         textView.setText(Float.toString(stationSelected));
         } else if (radioSelected.equals("AM")) {
         stationSelected = rand.nextInt(340) + 100;
         textView.setText(Float.toString(stationSelected));
         }*/
        scanBackward(view);
    }

    public void gotoPreset(View view){
        //find the correct view
        for(int i=0; i<PRESET_COUNT; i++) {
            if(mPresetViews[i].equals(view)){
                float tempStation;
                String info = "preset" + (i + 1);
                tempStation = mSharedPref.getFloat(info, -1);
                if(tempStation != -1) {
                    mStationSelected = tempStation;
                    TextView textView = (TextView) mView.findViewById(R.id.station_number);
                    textView.setText(Float.toString(mStationSelected));
                    info += "_type";
                    mRadioSelected = mSharedPref.getString(info, "No Radio");
                    textView = (TextView) mView.findViewById(R.id.radio_type);
                    textView.setText(mRadioSelected);

                    if(mRadioSelected.equals("FM")) {
                        mPrefEdit.putFloat(KEY_LAST_STATION_FM, mStationSelected);
                        mPrefEdit.commit();
                    }
                    else if(mRadioSelected.equals("AM")) {
                        mPrefEdit.putFloat(KEY_LAST_STATION_AM, mStationSelected);
                        mPrefEdit.commit();
                    }
                }
            }
        }
    }

    public void savePreset(View view){
        //find the correct view
        for(int i=0; i<PRESET_COUNT; i++) {
            if(mPresetViews[i].equals(view)){
                //save new preset into storage
                if(mStationSelected!=-1) {
                    String info = "preset" + (i + 1);
                    mPrefEdit.putFloat(info, mStationSelected);
                    info += "_type";
                    mPrefEdit.putString(info, mRadioSelected);
                    mPrefEdit.commit();

                    //change preset button text
                    TextView textView = (TextView) mPresetViews[i];
                    textView.setText(Float.toString(mStationSelected));
                    textView = (TextView) mPresetTypeViews[i];
                    textView.setText(mRadioSelected);
                }
            }
        }
    }

    public void loadPresets() {
        String radioType;
        float radioStation;
        int viewID;
        for(int i=0; i<PRESET_COUNT; i++){
            //get the station for preset i
            String stationID = "preset" + (i + 1);
            radioStation = mSharedPref.getFloat(stationID, -1);

            //gets the view's for each preset and their type text
            viewID = getResources().getIdentifier(stationID, "id",
                    mRadioActivity.getCarActivityService().getPackageName());
            mPresetViews[i] = mView.findViewById(viewID);
            //////debug code start
            //View pvtemp1 = (TextView) findViewById(viewID);
            //int temp1 = R.id.preset1;
            //int temp2 = R.id.preset2;
            //int temp3 = R.id.preset3;
            //View vtemp1 = findViewById(temp1);
            //View vtemp2 = findViewById(temp2);
            //View vtemp3 = findViewById(temp3);
            //TextView tv1 = (TextView) vtemp1;
            //TextView tv2 = (TextView) vtemp2;
            //TextView tv3 = (TextView) vtemp3;
            //String stemp1 = getResources().getResourceEntryName(R.id.preset1);
            //String stemp2 = getResources().getResourceTypeName(R.id.preset1);
            ////debug code end
            stationID += "_type";
            viewID = getResources().getIdentifier(stationID, "id",
                    mRadioActivity.getCarActivityService().getPackageName());
            mPresetTypeViews[i] = mView.findViewById(viewID);

            //do nothing if no preset
            if(radioStation != -1) {
                //change text for preset
                TextView textView = (TextView) mPresetViews[i];
                textView.setText(Float.toString(radioStation));

                //change what radio type the preset is
                radioType = mSharedPref.getString(stationID, "No Type");
                textView = (TextView) mPresetTypeViews[i];
                textView.setText(radioType);
            }

            //set up preset buttons for changing the station
            mPresetViews[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoPreset(v);
                }
            });

            //set up preset buttons for saving a station as a preset
            mPresetViews[i].setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    savePreset(v);
                    return true;
                }
            });
        }
    }
}
