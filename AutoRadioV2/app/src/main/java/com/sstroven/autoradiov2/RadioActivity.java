package com.sstroven.autoradiov2;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.car.support.Fragment;
import com.google.android.gms.car.support.FragmentTransaction;
import com.google.android.projection.sdk.CarActivityBase;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.view.View;

import com.google.android.projection.sdk.menu.CarMenu;
import com.google.android.projection.sdk.menu.CarMenuCallbacks;
import com.google.android.projection.sdk.menu.Root;

public class RadioActivity extends CarActivityBase {

    /**private static final String RESTORE_COUNT_KEY = "RESTORE_COUNT_KEY";
    private static final String ANDROID_AUTO_SDK_SAMPLE = "Android Auto SDK Sample";

    private final Handler mHandler = new Handler();

    private int mRestoreCount;
    private boolean mShowingTitle;*/

    private RadioActivityFragment mRadioFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRadioFragment = new RadioActivityFragment();
        setContentFragment(mRadioFragment);
    }

    /**protected void openSourceFragment() {
        mFragmentTransaction.remove(mRadioFragment);
        mFragmentTransaction.replace(R.id.fragment_radio, mSourceFragment);
        mFragmentTransaction.commit();
    }

    protected void openRadioFragment() {
        mFragmentTransaction.remove(mSourceFragment);
        mFragmentTransaction.replace(R.id.fragment_source, mRadioFragment);
        mFragmentTransaction.commit();
    }*/

    //not needed yet
    /**@Override
    protected void onResume() {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.sample_apps_bg);
        setBackground(bitmap);
        super.onResume();
    }*/

    //not needed yet
    /**
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mRestoreCount = savedInstanceState.getInt(RESTORE_COUNT_KEY);
        }
        mCurrentFragment.updateRestoreCount();
    }*/

    //not needed yet
    /**
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(RESTORE_COUNT_KEY, ++mRestoreCount);
    }*/

    //not needed yet
    /**protected int getRestoreCount() {
        return mRestoreCount;
    }*/

    //protected void switchFragment() {
    //    if (mCurrentFragment.equals(mRadioFragment)) {
    //        showRadioFragment();
    //    } else {
    //        showSourceFragment();
    //    }
    //}

    //TODO: fix this thing
    private void showRadioFragment() {
        if(mRadioFragment == null) {
            //mRadioFragment = createFragment("Radio Fragment"); //not needed currently
            mRadioFragment = new RadioActivityFragment();
        }
        //setContentFragment(mRadioFragment);
        //mCurrentFragment = mRadioFragment;
    }

    //private void showSourceFragment() {
    //    if(mSourceFragment == null) {
    //        //mRadioFragment = createFragment("Radio Fragment"); //not needed currently
    //        mSourceFragment = new SourceFragment();
    //    }
    //    setContentFragment(mSourceFragment);
    //    mCurrentFragment = mSourceFragment;
    //}

    //don't think this is needed
    /**private SdkSampleFragment createFragment(String name) {
        SdkSampleFragment fragment = new SdkSampleFragment();
        Bundle args = new Bundle();
        args.putString(SdkSampleFragment.NAME_KEY, name);
        fragment.setArguments(args);
        return fragment;
    }

    private mRadioFragment createFragment(String name) {

    }*/


    /**@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_radio, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
