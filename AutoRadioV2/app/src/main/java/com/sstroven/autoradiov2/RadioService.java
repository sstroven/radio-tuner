package com.sstroven.autoradiov2;

import com.google.android.gms.car.CarActivity;
import com.google.android.gms.car.CarActivityService;

public class RadioService extends CarActivityService {
    @Override
    protected Class<? extends CarActivity> getCarActivity() {
        return RadioActivity.class;
    }
}
