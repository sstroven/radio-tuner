package com.sstroven.autoradio;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Random;


public class MainActivity extends Activity {
    private SharedPreferences mSharedPref;
    private SharedPreferences.Editor mPrefEdit;
    private String mRadioSelected;
    private float mStationSelected;
    private final int PRESET_COUNT = 8;
    private View mPresetViews[] = new View[PRESET_COUNT];
    private View mPresetTypeViews[] = new View[PRESET_COUNT];
    private final String KEY_LAST_STATION = "last_station";
    private final String KEY_LAST_RADIO = "last_radio";
    private final String KEY_LAST_STATION_FM = "last_station_FM";
    private final String KEY_LAST_STATION_AM = "last_station_AM";

    public void setRadioFM(View view) {
        TextView textView = (TextView) findViewById(R.id.radio_type);
        textView.setText(R.string.radio_FM);
        mRadioSelected = "FM";
        textView = (TextView) findViewById(R.id.station_number);
        mStationSelected = mSharedPref.getFloat(KEY_LAST_STATION_FM, (float)88.7);
        textView.setText(Float.toString(mStationSelected));
    }

    public void setRadioAM(View view) {
        TextView textView = (TextView) findViewById(R.id.radio_type);
        textView.setText(R.string.radio_AM);
        mRadioSelected = "AM";
        textView = (TextView) findViewById(R.id.station_number);
        mStationSelected = mSharedPref.getFloat(KEY_LAST_STATION_AM, 330);
        textView.setText(Float.toString(mStationSelected));
    }

    public void scanForward(View view) {
        TextView textView = (TextView) findViewById(R.id.station_number);

        //temporary simulation
        Random rand = new Random();
        if(mRadioSelected.equals("FM")) {
            mStationSelected = rand.nextInt(30) + 80;
            textView.setText(Float.toString(mStationSelected));
            mPrefEdit.putFloat(KEY_LAST_STATION_FM, mStationSelected);
            mPrefEdit.commit();
        }
        else if(mRadioSelected.equals("AM")) {
            mStationSelected = rand.nextInt(340) + 100;
            textView.setText(Float.toString(mStationSelected));
            mPrefEdit.putFloat(KEY_LAST_STATION_AM, mStationSelected);
            mPrefEdit.commit();
        }
    }

    public void scanBackward(View view) {
        TextView textView = (TextView) findViewById(R.id.station_number);

        //temporary simulation
        Random rand = new Random();
        if(mRadioSelected.equals("FM")) {
            mStationSelected = rand.nextInt(30) + 80;
            textView.setText(Float.toString(mStationSelected));
            mPrefEdit.putFloat(KEY_LAST_STATION_FM, mStationSelected);
            mPrefEdit.commit();
        }
        else if(mRadioSelected.equals("AM")) {
            mStationSelected = rand.nextInt(340) + 100;
            textView.setText(Float.toString(mStationSelected));
            mPrefEdit.putFloat(KEY_LAST_STATION_AM, mStationSelected);
            mPrefEdit.commit();
        }
    }

    public void seekForward(View view) {
        TextView textView = (TextView) findViewById(R.id.station_number);

        //temporary simulation
        Random rand = new Random();
        /**if (radioSelected.equals("FM")) {
            stationSelected = rand.nextInt(30) + 80;
            textView.setText(Float.toString(stationSelected));
        } else if (radioSelected.equals("AM")) {
            stationSelected = rand.nextInt(340) + 100;
            textView.setText(Float.toString(stationSelected));
        }*/
        scanForward(view);
    }

    public void seekBackward(View view) {
        TextView textView = (TextView) findViewById(R.id.station_number);

        //temporary simulation
        /**Random rand = new Random();
        if (radioSelected.equals("FM")) {
            stationSelected = rand.nextInt(30) + 80;
            textView.setText(Float.toString(stationSelected));
        } else if (radioSelected.equals("AM")) {
            stationSelected = rand.nextInt(340) + 100;
            textView.setText(Float.toString(stationSelected));
        }*/
        scanBackward(view);
    }

    public void gotoPreset(View view){
        //find the correct view
        for(int i=0; i<PRESET_COUNT; i++) {
            if(mPresetViews[i].equals(view)){
                float tempStation;
                String info = "preset" + (i + 1);
                tempStation = mSharedPref.getFloat(info, -1);
                if(tempStation != -1) {
                    mStationSelected = tempStation;
                    TextView textView = (TextView) findViewById(R.id.station_number);
                    textView.setText(Float.toString(mStationSelected));
                    info += "_type";
                    mRadioSelected = mSharedPref.getString(info, "No Radio");
                    textView = (TextView) findViewById(R.id.radio_type);
                    textView.setText(mRadioSelected);

                    if(mRadioSelected.equals("FM")) {
                        mPrefEdit.putFloat(KEY_LAST_STATION_FM, mStationSelected);
                        mPrefEdit.commit();
                    }
                    else if(mRadioSelected.equals("AM")) {
                        mPrefEdit.putFloat(KEY_LAST_STATION_AM, mStationSelected);
                        mPrefEdit.commit();
                    }
                }
            }
        }
    }

    public void savePreset(View view){
        //find the correct view
        for(int i=0; i<PRESET_COUNT; i++) {
            if(mPresetViews[i].equals(view)){
                //save new preset into storage
                String info = "preset" + (i + 1);
                if(mStationSelected != -1) {
                    mPrefEdit.putFloat(info, mStationSelected);
                    info += "_type";
                    mPrefEdit.putString(info, mRadioSelected);
                    mPrefEdit.commit();

                    //change preset button text
                    TextView textView = (TextView) mPresetViews[i];
                    textView.setText(Float.toString(mStationSelected));
                    textView = (TextView) mPresetTypeViews[i];
                    textView.setText(mRadioSelected);
                }
            }
        }
    }

    public void loadPresets() {
        String radioType;
        float radioStation;
        int viewID;
        for(int i=0; i<PRESET_COUNT; i++){
            //get the station for preset i
            String stationID = "preset" + (i + 1);
            radioStation = mSharedPref.getFloat(stationID, -1);

            //gets the view's for each preset and their type text
            viewID = getResources().getIdentifier(stationID, "id", getPackageName());
            mPresetViews[i] = findViewById(viewID);
            stationID += "_type";
            viewID = getResources().getIdentifier(stationID, "id", getPackageName());
            mPresetTypeViews[i] = findViewById(viewID);

            //do nothing if no preset
            if(radioStation != -1) {
                //change text for preset
                TextView textView = (TextView) mPresetViews[i];
                textView.setText(Float.toString(radioStation));

                //change what radio type the preset is
                radioType = mSharedPref.getString(stationID, "No Type");
                textView = (TextView) mPresetTypeViews[i];
                textView.setText(radioType);
            }

            //set up preset buttons for changing the station
            mPresetViews[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoPreset(v);
                }
            });

            //set up preset buttons for saving a station as a preset
            mPresetViews[i].setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    savePreset(v);
                    return true;
                }
            });
        }
    }

    public void gotoRadioSources(View view) {
        final LinearLayout radioFragment = (LinearLayout) this.findViewById(R.id.radio_fragment);
        final LinearLayout sourceFragment = (LinearLayout) this.findViewById(R.id.source_fragment);
        radioFragment.setVisibility(View.INVISIBLE);
        sourceFragment.setVisibility(View.VISIBLE);

        Button button = (Button) this.findViewById(R.id.radio_select_FM);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioFragment.setVisibility(View.VISIBLE);
                sourceFragment.setVisibility(View.INVISIBLE);
                setRadioFM(view);
            }
        });
        button = (Button) this.findViewById(R.id.radio_select_AM);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioFragment.setVisibility(View.VISIBLE);
                sourceFragment.setVisibility(View.INVISIBLE);
                setRadioAM(view);
            }
        });
        button = (Button) this.findViewById(R.id.radio_select_back);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                radioFragment.setVisibility(View.VISIBLE);
                sourceFragment.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSharedPref = getPreferences(MODE_PRIVATE);
        mPrefEdit = mSharedPref.edit();
        mStationSelected = -1;
        mRadioSelected = "No Radio";

        //load up saves
        mStationSelected = mSharedPref.getFloat(KEY_LAST_STATION, -1);
        mRadioSelected = mSharedPref.getString(KEY_LAST_RADIO, "No Radio");
        if(mStationSelected != -1) {
            TextView textView = (TextView) findViewById(R.id.station_number);
            textView.setText(Float.toString(mStationSelected));
            textView = (TextView) findViewById(R.id.radio_type);
            textView.setText(mRadioSelected);
        }
        loadPresets();

        Button button;
        button = (Button) this.findViewById(R.id.radio_sources);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoRadioSources(view);
            }
        });
        button = (Button) this.findViewById(R.id.seek_forward);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seekForward(view);
            }
        });
        button = (Button) this.findViewById(R.id.seek_backward);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seekBackward(view);
            }
        });
        button = (Button) this.findViewById(R.id.scan_forward);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanForward(view);
            }
        });
        button = (Button) this.findViewById(R.id.scan_backward);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scanBackward(view);
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPrefEdit.putFloat(KEY_LAST_STATION, mStationSelected);
        mPrefEdit.putString(KEY_LAST_RADIO, mRadioSelected);
        mPrefEdit.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}

